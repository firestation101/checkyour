def view(env, viewfile)
  env.response.content_type = "text/html"
  env.response.content_length = File.size(viewfile)
  File.open(viewfile) do |file|
    IO.copy(file, env.response)
  end

  ""
end
